package com.airport.util;

public interface Constant {
	public static final char   TERM_GATE_SEPERATOR = '-';
	public static final String NEW_LINE            = "\n";
	public static final String EMPTY_STRING        = "";

	public static final String ONTIME    = "ON TIME";
	public static final String BOARDING  = "BOARDING";
	public static final String DELAYED   = "DELAYED";
	public static final String CANCELLED = "CANCELLED";
	
	public static final String SCREEN_EMPTY_LIST = "---";
	
	/* Airport */
	public static final String AIRPORT_NAME = "Airport Simulation";
	
	/* Size Terminal */
	public static final int SIZE_TERMINAL_A = 3;
	public static final int SIZE_TERMINAL_B = 7;
	public static final int SIZE_TERMINAL_C = 5;
	
	/* Terminal */
	public static final String TERM_A = "A";
	public static final String TERM_B = "B";
	public static final String TERM_C = "C";
	
	public static final String TERMINAL_A = "TERMINAL A";
	public static final String TERMINAL_B = "TERMINAL B";
	public static final String TERMINAL_C = "TERMINAL C";
	
	/* Screen Names */
	public static final String AIRPORT_1 = "AIRPORT (1)";
	public static final String AIRPORT_2 = "AIRPORT (2)";

	public static final String TERMINAL_A_1 = "TERMINAL A (1)";
	public static final String TERMINAL_A_2 = "TERMINAL A (2)";
	public static final String TERMINAL_A_3 = "TERMINAL A (3)";
	public static final String TERMINAL_B_1 = "TERMINAL B (1)";
	public static final String TERMINAL_B_2 = "TERMINAL B (2)";
	public static final String TERMINAL_B_3 = "TERMINAL B (3)";
	public static final String TERMINAL_C_1 = "TERMINAL C (1)";
	public static final String TERMINAL_C_2 = "TERMINAL C (2)";
	public static final String TERMINAL_C_3 = "TERMINAL C (3)";
	
	/* Screen Name */
	public static final String SCREEN_ADDFLIGHT      = "Add Flight";
	public static final String SCREEN_DELAYFLIGHT    = "Delay Flight";
	public static final String SCREEN_REMOVEFLIGHT   = "Remove Flight";
	public static final String SCREEN_NOTIFYBOARDING = "Board Flight";
	public static final String SCREEN_CHANGEGATE     = "Change a Flight gate";
	public static final String SCREEN_CANCELFLIGHT   = "Cancel Flight";
	public static final String SCREEN_ERROR          = "Error";
	
	/* Button Name */
	public static final String BUTTON_ADDFLIGHTTEXT      = "Add Flight";
	public static final String BUTTON_DELAYFLIGHTTEXT    = "Delay Flight";
	public static final String BUTTON_REMOVEFLIGHTTEXT   = "Remove Flight";
	public static final String BUTTON_NOTIFYBOARDINGTEXT = "Board Flight";
	public static final String BUTTON_CHANGEGATETEXT     = "Change a Flight gate";
	public static final String BUTTON_CANCELFLIGHTTEXT   = "Cancel Flight";
	public static final String BUTTON_QUITTEXT           = "Quit";
	public static final String BUTTON_OKTEXT             = "OK";
	public static final String BUTTON_CANCELTEXT         = "Cancel";
	
	/* Display Area */
	public static final String DISPLAYAREA_ADDFLIGHTTEXT      = "Add Flight Option";
	public static final String DISPLAYAREA_DELAYFLIGHTTEXT    = "Delay a Flight Option";
	public static final String DISPLAYAREA_REMOVEFLIGHTTEXT   = "Remove a Flight Option";
	public static final String DISPLAYAREA_NOTIFYBOARDINGTEXT = "Notify the boarding of a Flight Option";
	public static final String DISPLAYAREA_CHANGEGATETEXT     = "Change a flight's gate Option";
	public static final String DISPLAYAREA_CANCELFLIGHTTEXT   = "Cancel a Flight Option";
	public static final String DISPLAYAREA_QUITTEXT           = "Quit";
	
	/* Display area Name */
	public static final String DISPLAY_COMPANY       = "Company: ";
	public static final String DISPLAY_FLIGHTNUMBER  = "Flight Number: ";
	public static final String DISPLAY_DESTINATION   = "Destination: ";
	public static final String DISPLAY_DEPARTURETIME = "Departure Time: ";
	public static final String DISPLAY_TERMINAL      = "Terminal: ";
	public static final String DISPLAY_GATENUMBER    = "Gate Number: ";
	public static final String DISPLAY_FLIGHTSTATUS  = "Flight Status: ";
	public static final String DISPLAY_NEWTERMINAL   = "New Terminal (A,B,C): ";
	public static final String DISPLAY_NEWGATE       = "New Gate Number: ";
	
	/* Display Menu */
	public static final String CHOICE_HEADER 	= "Select Option: ";
	public static final String CHOICE_1 		= "1 - Add Flight";
	public static final String CHOICE_2 		= "2 - Delay Flight";
	public static final String CHOICE_3 		= "3 - Change Gate";
	public static final String CHOICE_4 		= "4 - Cancel Flight";
	public static final String CHOICE_5 		= "5 - Notify Boarding";
	public static final String CHOICE_6 		= "6 - Remove Flight";
	public static final String CHOICE_0 		= "0 - Quit";
	public static final String CHOICE_ERROR 	= "Error - stop";
	public static final String CHOICE_SEPARATOR = "********************";
	
	/* Status Menu */
	public static final String STATUS_HEADER 	= "Status: ";
	public static final String STATUS_1 		= "1 - ON TIME";
	public static final String STATUS_2 		= "2 - CANCELLED";
	public static final String STATUS_3 		= "3 - BOARDING";
	public static final String STATUS_4 		= "4 - DELAYED";

	/* Cancel & Quit Features */
	public static final String CONFIRM_TEXT      = "Do you want to cancel?";
	public static final String CONFIRM_QUIT_TEXT = "Do you want to quit?";
	public static final String QUIT_TEXT         = "quit";
	public static final String CANCEL_TEXT       = "Cancel";
	
	/* Message */
	public static final String FLIGHT_REMOVED        = "\nThe flight was removed successfully\n";
	public static final String FLIGHT_ADDED          = "\nFlight added\n";
	public static final String FLIGHT_STATUS_CHANGED = "\nFlight status was changed successfully\n";
	public static final String FLIGHT_GATE_CHANGED   = "\nThe flight gate was changed successfully\n";
			
	/* Error Message (Some with the help of https://honk.moe/tools/owo.html) */
	public static final String NO_ERROR                     = "";
	public static final String ERROR                        = "ERROR";
	public static final String ERROR_ADDFLIGHT_ALREADYEXIST = "The flight awweady exist in the aiwpowt. Pwease twy again.";
	public static final String ERROR_ADDFLIGHT_WRONGGATE    = " is a nyon-existant gate ow is nyot avaiwabwe. Pwease twy again with anyothew gate.";
	public static final String ERROR_CHANGESTATUS_NOTFOUND  = "Status not changed";
	public static final String ERROR_FLIGHTNOTFOUND         = "Flight nyot fwound";
	public static final String ERROR_GATEUSED               = " is nyot avaiwabwe. Pwease twy again with an empty gate";
	public static final String ERROR_GATENOTAVAILABLE       = "The new gate is nyot avaiwabwe";
	public static final String ERROR_SAMEGATE               = "ERROR : Same gate than before";
}
