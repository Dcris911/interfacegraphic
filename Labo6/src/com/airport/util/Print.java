package com.airport.util;
import java.io.BufferedWriter;
import java.io.FileWriter;
import java.io.IOException;
import java.io.PrintWriter;

import com.airport.core.AirportMainframe;
import com.airport.core.Flight;
import com.airport.subject.Gate;

public class Print {

	private AirportMainframe d;

	public Print(AirportMainframe driver) {
		super();
		this.d = driver;
	}

	/** Found : https://stackoverflow.com/questions/1053467/how-do-i-save-a-string-to-a-text-file-using-java */
	public void sendToFile(String s) {
		PrintWriter pwFile1 = null;
		try {
			FileWriter fw = new FileWriter("test.txt", true);
			BufferedWriter br = new BufferedWriter(fw);
			pwFile1 = new PrintWriter(br) ;       
			pwFile1.println(s);
		} catch (IOException e) {
			// for FileWriter
			e.printStackTrace();
		}finally {
			if(pwFile1 != null){
				pwFile1.close();
		   }
		}
	}
	
	public void printAirport() {
		sendToFile(d.getAirport().getFlights().toString());
	}

	public void printTerminal() {
		String s = "GATE A : [";
		for(Gate gate : d.getGatesA()) {
			s+= gate.getFlights().toString() + ",";
		}
		s+= "]";
		sendToFile(s);
		s = "GATE B : [";
		for(Gate gate : d.getGatesB()) {
			s+= gate.getFlights().toString() + ",";
		}
		s+= "]";
		sendToFile(s);
		s = "GATE C : [";
		for(Gate gate : d.getGatesC()) {
			s+= gate.getFlights().toString() + ",";
		}
		s+= "]";
		sendToFile(s);
	}	
	
	public void printAll() {
		//printAirport();
		printTerminal();
		//printGate(Gate gate);
	}
	
	public static void printLogo() {
		//https://www.asciiart.eu/nature/clouds
		System.out.println("        .-~~~-.");
		System.out.println(".- ~ ~-(       )_ _");
		System.out.println("/                     ~ -.");
		System.out.println("|    Bienvenue chez        \\");
		System.out.println("\\       Nuage en Aire    .'");
		System.out.println(" ~- . _____________ . -~");
	}
}
