package com.airport.observer;

import com.airport.Dialog.ScreenDialog;
import com.airport.subject.Subject;

public class TerminalScreen extends Observer{

	private String terminalName;

	public TerminalScreen(Subject terminal, String nameScreen, ScreenDialog sd) {
		super(terminal, sd);
		this.terminalName = nameScreen;
	}

	@Override
	public String getScreenName() {
		return terminalName;
	}
	
}
