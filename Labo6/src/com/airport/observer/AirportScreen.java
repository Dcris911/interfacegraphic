package com.airport.observer;

import com.airport.Dialog.ScreenDialog;
import com.airport.subject.Subject;

public class AirportScreen extends Observer{
	
	private String airportName;
	
	public AirportScreen(Subject airport, String airportName, ScreenDialog sd) {
		super(airport, sd);
		this.airportName = airportName;
	}

	@Override
	public String getScreenName() {
		return airportName;
	}
	
}