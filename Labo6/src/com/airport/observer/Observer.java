package com.airport.observer;

import java.util.List;

import com.airport.Dialog.ScreenDialog;
import com.airport.core.Flight;
import com.airport.subject.Subject;
import com.airport.util.Constant;

public abstract class Observer {

	protected Subject subject;
	protected ScreenDialog screenDialog;

	protected Observer(Subject subject, ScreenDialog screenDialog){
		this.subject = subject;
		this.screenDialog = screenDialog;
	}
	
	public void update() {
		Observer.displayFlights(subject.getFlights(), screenDialog);
	}

	public static void displayFlights(List<Flight> flights, ScreenDialog sd) {
		if(flights.isEmpty()) {
			sd.setScreenText(Constant.SCREEN_EMPTY_LIST);
		} else {
			StringBuilder sb = new StringBuilder();
			for(Flight temp : flights) {
				sb.append(temp + Constant.NEW_LINE);
			}
			sd.setScreenText(sb.toString());			
		}
	}
	
	public abstract String getScreenName();
}
