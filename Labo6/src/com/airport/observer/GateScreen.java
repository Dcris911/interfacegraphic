package com.airport.observer;

import com.airport.Dialog.ScreenDialog;
import com.airport.subject.Gate;
import com.airport.subject.Subject;
import com.airport.util.Constant;

public class GateScreen extends Observer{
	
	public GateScreen(Subject gate, ScreenDialog sd) {
		super(gate, sd);
	}

	@Override
	public String getScreenName() {
		if(super.subject instanceof Gate) {
			return ((Gate) super.subject).printDoor();
		}
		return Constant.EMPTY_STRING;
	}
	
}