package com.airport.subject;

import com.airport.core.Flight;

public class Gate extends Subject{
	private String terminalDoor;

	public Gate(String terminalDoor) {
		super();
		this.terminalDoor = terminalDoor;
	}

	@Override
	public void addFlight(Flight flight) {
		flights.add(flight);
		notifyObservers();
	}

	@Override
	public void removeFlight(Flight flight) {
		flights.remove(flight);
		notifyObservers();
	}
	
	public String printDoor() {
		return "GATE " + terminalDoor;
	}
}