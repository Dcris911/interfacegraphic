package com.airport.subject;

import com.airport.core.Flight;

public class Terminal extends Subject{
	
	public Terminal(String uselessString) {
		super();
	}

	@Override
	public void addFlight(Flight flight) {
		flights.add(flight);
		notifyObservers();
	}

	@Override
	public void removeFlight(Flight flight) {
		flights.remove(flight);
		notifyObservers();
	}
	
}