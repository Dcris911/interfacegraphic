package com.airport.subject;
import java.util.LinkedList;
import java.util.List;

import com.airport.core.Flight;
import com.airport.observer.Observer;

public abstract class Subject {
  
  private List<Observer> listObservers = new LinkedList<Observer>();
  protected List<Flight> flights;
  
  protected Subject() {
	super();
	this.flights = new LinkedList<Flight>();
}

  public void attach(Observer o) {
    listObservers.add(o);
  }
  
  public void detach(Observer o) {
    listObservers.remove(o);
  }
  
  public void notifyObservers() {
    for (Observer obv : listObservers) {
      obv.update();
    }
  }

  public List<Flight> getFlights(){
	  return flights;
  }
  
  abstract public void addFlight(Flight flight);
  abstract public void removeFlight(Flight flight);

}
