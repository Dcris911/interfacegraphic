package com.airport.subject;

import com.airport.core.Flight;

public class Airport extends Subject{

	public Airport() {
		super();
	}

	@Override
	public void addFlight(Flight flight) {
		flights.add(flight);
		notifyObservers();
		
	}

	@Override
	public void removeFlight(Flight flight) {
		flights.remove(flight);
		notifyObservers();
	}

	/**
	 * See if the flight is in the airport
	 * @param companyName The abridged name of the company
	 * @param flightNumber The id of the flight
	 * @return the flight if found, or null if not
	 */
	public Flight getFlightById(String companyName, int flightNumber) {
		for(Flight temp : flights) {
			if(temp.getFlightNumber() == flightNumber) {
				if(!companyName.isEmpty() && companyName.equalsIgnoreCase(temp.getCompany())) {
					return temp;					
				}
			}
		}
		return null;
	}
	
}
