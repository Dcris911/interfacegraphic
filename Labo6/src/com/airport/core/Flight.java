package com.airport.core;

import com.airport.util.Constant;

public class Flight{
	private String company;
	private int flightNumber;
	private String destination;
	private int departureTime;
	private String gate;
	private String status;
	
	public final static String ONTIME = Constant.ONTIME;
	public final static String BOARDING = Constant.BOARDING;
	public final static String DELAYED = Constant.DELAYED;
	public final static String CANCELLED = Constant.CANCELLED;
	
	public Flight(String company, int flightNumber, String destination, int departureTime, String gate, String status) {
		super();
		this.company = company;
		this.flightNumber = flightNumber;
		this.destination = destination;
		this.departureTime = departureTime;
		this.gate = gate;
		this.status = status;
	}
	
	public String getCompany() {
		return company;
	}
	public int getFlightNumber() {
		return flightNumber;
	}
	public String getDestination() {
		return destination;
	}
	public int getDepartureTime() {
		return departureTime;
	}
	public String getGate() {
		return gate;
	}
	public String getStatus() {
		return status;
	}
	public void setCompany(String company) {
		this.company = company;
	}
	public void setFlightNumber(int flightNumber) {
		this.flightNumber = flightNumber;
	}
	public void setDestination(String destination) {
		this.destination = destination;
	}
	public void setDepartureTime(int departureTime) {
		this.departureTime = departureTime;
	}
	public void setGate(String gate) {
		this.gate = gate;
	}
	public void setStatus(String status) {
		this.status = status;
	}
	
	@Override
	public String toString() {
		return this.company+this.flightNumber+" "+this.destination+" "+this.departureTime+" "+this.gate+" "+this.status;
	}

	@Override
	public boolean equals(Object obj) {
		if(obj != null) {
			if(obj instanceof Flight) {
				Flight temp = (Flight) obj;
				if(this.company.equalsIgnoreCase(temp.company)
					&& this.flightNumber == temp.flightNumber
					&& this.destination.equalsIgnoreCase(temp.destination)
					&& this.departureTime == temp.departureTime
					&& this.gate.equalsIgnoreCase(temp.gate)
					&& this.status.equalsIgnoreCase(temp.status)) {
						return true;
				}
			}
		}
		return false;
	}

	public String getTerminalFromGate(){
		int temp = this.gate.indexOf(Constant.TERM_GATE_SEPERATOR);
		if(temp != -1) {
			return this.gate.substring(0, temp);
		}
		return Constant.EMPTY_STRING;
	}
	public int getGateNumberFromGate(){
		return Integer.parseInt(getGateNumberFromGateAsString());
	}
	
	private String getGateNumberFromGateAsString(){
		int temp = this.gate.indexOf(Constant.TERM_GATE_SEPERATOR);
		if( (temp != -1) && (temp < this.gate.length()) ) {
			return this.gate.substring(temp+1, this.gate.length());
		}
		return Constant.EMPTY_STRING;
	}
}