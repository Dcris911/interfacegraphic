package com.airport.core;

import java.awt.*;
import java.awt.event.*;
import javax.swing.*;

import com.airport.Dialog.*;
import com.airport.observer.AirportScreen;
import com.airport.observer.GateScreen;
import com.airport.observer.TerminalScreen;
import com.airport.subject.Airport;
import com.airport.subject.Gate;
import com.airport.subject.Terminal;
import com.airport.util.Constant;

@SuppressWarnings("serial")
public class Simulation extends JFrame {

	/*
	 * Simulation attributes DO NOT EDIT
	 */
	AirportMainframe amf = new AirportMainframe();
	
	Airport  airport = amf.getAirport();
	Terminal termA   = amf.getTermA();
	Terminal termB   = amf.getTermB();
	Terminal termC   = amf.getTermC();
	Gate[]   gatesA  = amf.getGatesA();
	Gate[]   gatesB  = amf.getGatesB();
	Gate[]   gatesC  = amf.getGatesC();
	
	private ScreenDialog airportScreen1Dialog = new ScreenDialog(this, Constant.AIRPORT_1, 0);
	private ScreenDialog airportScreen2Dialog = new ScreenDialog(this, Constant.AIRPORT_2, 0);
	private ScreenDialog termAScreen1Dialog = new ScreenDialog(this, Constant.TERMINAL_A_1, 0);
	private ScreenDialog termAScreen2Dialog = new ScreenDialog(this, Constant.TERMINAL_A_2, 0);
	private ScreenDialog termAScreen3Dialog = new ScreenDialog(this, Constant.TERMINAL_A_3, 0);
	private ScreenDialog termBScreen1Dialog = new ScreenDialog(this, Constant.TERMINAL_B_1, 0);
	private ScreenDialog termBScreen2Dialog = new ScreenDialog(this, Constant.TERMINAL_B_2, 0);
	private ScreenDialog termBScreen3Dialog = new ScreenDialog(this, Constant.TERMINAL_B_3, 0);
	private ScreenDialog termCScreen1Dialog = new ScreenDialog(this, Constant.TERMINAL_C_1, 0);
	private ScreenDialog termCScreen2Dialog = new ScreenDialog(this, Constant.TERMINAL_C_2, 0);
	private ScreenDialog termCScreen3Dialog = new ScreenDialog(this, Constant.TERMINAL_C_3, 0);
	private ScreenDialog[] gatesADialog = new ScreenDialog[Constant.SIZE_TERMINAL_A];
	private ScreenDialog[] gatesBDialog = new ScreenDialog[Constant.SIZE_TERMINAL_B];
	private ScreenDialog[] gatesCDialog = new ScreenDialog[Constant.SIZE_TERMINAL_C];

	/**
	 * GUI attributes
	 */
	public static final int WIDTH = 850;
	public static final int HEIGHT = 300;
	private static final int LINE_COUNT = 12;
	private static final int LINE_SIZE = 50; // in characters

	private JButton addButton, delayButton, cancelButton, boardingButton, removeButton, changeGateButton, quitButton;
	private JTextArea displayArea;
	private JScrollPane scrollPane;
	private Container contentPane;

	/**
	 * Creates the observers and attach them. DO NOT EDIT
	 */
	public void createObservers() {

        // Airport screens
		AirportScreen airportScreen1 = new AirportScreen(airport, Constant.AIRPORT_1, airportScreen1Dialog);
		AirportScreen airportScreen2 = new AirportScreen(airport, Constant.AIRPORT_2, airportScreen2Dialog);
		airport.attach(airportScreen1);
		airport.attach(airportScreen2);

		// Terminal screens
		TerminalScreen termAScreen1 = new TerminalScreen(termA, Constant.TERMINAL_A_1, termAScreen1Dialog);
		TerminalScreen termAScreen2 = new TerminalScreen(termA, Constant.TERMINAL_A_2, termAScreen2Dialog);
		TerminalScreen termAScreen3 = new TerminalScreen(termA, Constant.TERMINAL_A_3, termAScreen3Dialog);
		termA.attach(termAScreen1);
		termA.attach(termAScreen2);
		termA.attach(termAScreen3);

		TerminalScreen termBScreen1 = new TerminalScreen(termB, Constant.TERMINAL_B_1, termBScreen1Dialog);
		TerminalScreen termBScreen2 = new TerminalScreen(termB, Constant.TERMINAL_B_2, termBScreen2Dialog);
		TerminalScreen termBScreen3 = new TerminalScreen(termB, Constant.TERMINAL_B_3, termBScreen3Dialog);
		termB.attach(termBScreen1);
		termB.attach(termBScreen2);
		termB.attach(termBScreen3);

		TerminalScreen termCScreen1 = new TerminalScreen(termC, Constant.TERMINAL_C_1, termCScreen1Dialog);
		TerminalScreen termCScreen2 = new TerminalScreen(termC, Constant.TERMINAL_C_2, termCScreen2Dialog);
		TerminalScreen termCScreen3 = new TerminalScreen(termC, Constant.TERMINAL_C_3, termCScreen3Dialog);
		termC.attach(termCScreen1);
		termC.attach(termCScreen2);
		termC.attach(termCScreen3);

		// Gates 
		
		// Terminal A
		for (int i = 0; i < gatesA.length; ++i) {
			gatesA[i] = new Gate("A-" + (i + 1));
			gatesA[i].attach(new GateScreen(gatesA[i], gatesADialog[i]));
		}

		// Terminal B
		for (int i = 0; i < gatesB.length; ++i) {
			gatesB[i] = new Gate("B-" + (i + 1));
			gatesB[i].attach(new GateScreen(gatesB[i], gatesBDialog[i]));
		}

		// Terminal C
		for (int i = 0; i < gatesC.length; ++i) {
			gatesC[i] = new Gate("C-" + (i + 1));
			gatesC[i].attach(new GateScreen(gatesC[i], gatesCDialog[i]));
		}
	}

	/**
	 * Creates the objects for the airport screens
	 */
	public void createScreenDialogs() {
		airportScreen1Dialog.setVisible(true);
		airportScreen2Dialog.setVisible(true);

		termAScreen1Dialog.setVisible(true);
		termAScreen2Dialog.setVisible(true);
		termAScreen3Dialog.setVisible(true);
		termBScreen1Dialog.setVisible(true);
		termBScreen2Dialog.setVisible(true);
		termBScreen3Dialog.setVisible(true);
		termCScreen1Dialog.setVisible(true);
		termCScreen2Dialog.setVisible(true);
		termCScreen3Dialog.setVisible(true);

		for (int i = 0; i < gatesADialog.length; ++i) {
			gatesADialog[i] = new ScreenDialog(this, "GATE A-" + (i + 1), 0);
			gatesADialog[i].setVisible(true);
		}

		for (int i = 0; i < gatesBDialog.length; ++i) {
			gatesBDialog[i] = new ScreenDialog(this, "GATE B-" + (i + 1), 0);
			gatesBDialog[i].setVisible(true);
		}

		for (int i = 0; i < gatesCDialog.length; ++i) {
			gatesCDialog[i] = new ScreenDialog(this, "GATE C-" + (i + 1), 0);
			gatesCDialog[i].setVisible(true);
		}
	}

	/**
	 * Initializes the GUI components
	 */
	public void initComponents() {
		addButton        = new JButton(Constant.BUTTON_ADDFLIGHTTEXT);
		delayButton      = new JButton(Constant.BUTTON_DELAYFLIGHTTEXT);
		boardingButton   = new JButton(Constant.BUTTON_NOTIFYBOARDINGTEXT);
		cancelButton     = new JButton(Constant.BUTTON_CANCELFLIGHTTEXT);
		removeButton     = new JButton(Constant.BUTTON_REMOVEFLIGHTTEXT);
		changeGateButton = new JButton(Constant.BUTTON_CHANGEGATETEXT);
		quitButton       = new JButton(Constant.BUTTON_QUITTEXT);

		// Initialize display area
		displayArea = new JTextArea(LINE_COUNT, LINE_SIZE);
		displayArea.setEditable(false);
		scrollPane = new JScrollPane(displayArea, JScrollPane.VERTICAL_SCROLLBAR_ALWAYS, JScrollPane.HORIZONTAL_SCROLLBAR_ALWAYS);
	}

	public void initLayout() {
		contentPane = getContentPane();
		contentPane.setLayout(new FlowLayout());
	}

	public void initContentPane() {
		contentPane.add(addButton);
		contentPane.add(delayButton);
		contentPane.add(cancelButton);
		contentPane.add(boardingButton);
		contentPane.add(removeButton);
		contentPane.add(changeGateButton);
		contentPane.add(quitButton);

		scrollPane = new JScrollPane(displayArea, JScrollPane.VERTICAL_SCROLLBAR_AS_NEEDED, JScrollPane.HORIZONTAL_SCROLLBAR_AS_NEEDED);
		contentPane.add(scrollPane);
	}

	public void initListeners() {

		addButton.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				displayArea.append(Constant.DISPLAYAREA_ADDFLIGHTTEXT);
				System.out.println(Constant.DISPLAYAREA_ADDFLIGHTTEXT);
				AddFlightDialog addFlightDialog = new AddFlightDialog(Simulation.this);
				addFlightDialog.setVisible(true);
			}
		});
		
		delayButton.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				displayArea.append(Constant.DISPLAYAREA_DELAYFLIGHTTEXT);
				System.out.println(Constant.DISPLAYAREA_DELAYFLIGHTTEXT);
				AbstractAirportDialog delayFlightDialog = new DelayFlightDialog(Simulation.this);
				delayFlightDialog.setVisible(true);
			}
		});
		
		cancelButton.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				displayArea.append(Constant.DISPLAYAREA_CANCELFLIGHTTEXT);
				System.out.println(Constant.DISPLAYAREA_CANCELFLIGHTTEXT);
				AbstractAirportDialog cancelFlightDialog = new CancelFlightDialog(Simulation.this);
				cancelFlightDialog.setVisible(true);
			}
		});
		
		boardingButton.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				displayArea.append(Constant.DISPLAYAREA_NOTIFYBOARDINGTEXT);
				System.out.println(Constant.DISPLAYAREA_NOTIFYBOARDINGTEXT);
				AbstractAirportDialog boardingFlightDialog = new NotifyBoardingDialog(Simulation.this);
				boardingFlightDialog.setVisible(true);
			}
		});
		
		removeButton.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				displayArea.append(Constant.DISPLAYAREA_REMOVEFLIGHTTEXT);
				System.out.println(Constant.DISPLAYAREA_REMOVEFLIGHTTEXT);
				AbstractAirportDialog removeFlightDialog = new RemoveFlightDialog(Simulation.this);
				removeFlightDialog.setVisible(true);
			}
		});
		
		changeGateButton.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				displayArea.append(Constant.DISPLAYAREA_CHANGEGATETEXT);
				System.out.println(Constant.DISPLAYAREA_CHANGEGATETEXT);
				AbstractAirportDialog changeGateFlightDialog = new ChangeGateDialog(Simulation.this);
				changeGateFlightDialog.setVisible(true);
			}
		});
		
		quitButton.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				displayArea.append(Constant.DISPLAYAREA_QUITTEXT);
				System.out.println(Constant.DISPLAYAREA_QUITTEXT);

				// Yes/no pop-up
				int choice = JOptionPane.showConfirmDialog(null, Constant.CONFIRM_QUIT_TEXT, Constant.QUIT_TEXT,
						JOptionPane.YES_NO_OPTION, JOptionPane.QUESTION_MESSAGE);
				if (choice == JOptionPane.YES_OPTION) {
					System.exit(0);
				}
			}
		});

	}

	public void init() {
		initComponents();
		initLayout();
		initContentPane();
		initListeners();
	}

	public Simulation(String windowTitle) {
		super(windowTitle);

		frameInit();
		init();
		createScreenDialogs();
		createObservers();

		// Add listener to quit
		this.addWindowListener(new WindowAdapter() {
			public void windowClosing(WindowEvent e) {
				System.exit(0);
			}
		});

		pack(); // Start to change frame dimensions
		setVisible(true);
		setSize(WIDTH, HEIGHT);
		setResizable(true);
		validate(); // Update and validation
		notifyAllObservers();
	}

	private void notifyAllObservers() {
		airport.notifyObservers();
		termA.notifyObservers();
		termB.notifyObservers();
		termC.notifyObservers();
		for(Gate gate : gatesA) {
			gate.notifyObservers();
		}
		for(Gate gate : gatesB) {
			gate.notifyObservers();
		}
		for(Gate gate : gatesC) {
			gate.notifyObservers();
		}
	}

	public static void main(String[] args) {
		new Simulation(Constant.AIRPORT_NAME);

	}

	public void appendToDisplayTheResult(String errorMessage, String text) {
		if(errorMessage.isEmpty()) {
			displayArea.append(text + Constant.NEW_LINE);
		} else {
			JOptionPane.showMessageDialog(
					Simulation.this, 
					errorMessage, 
					Constant.ERROR, 
					JOptionPane.ERROR_MESSAGE
			);
		}
	}

	public String addFlight(Flight flight, String terminal, int gateNumber) {
		return amf.addFlight(flight, terminal, gateNumber);
	}
	
	public String removeFlight(String company, int flightNumber) {
		return amf.removeFlight(company, flightNumber);
	}
	
	public String delayFlight(String company, int flightNumber) {
		return amf.delayFlight(company, flightNumber);
	}

	public String cancelFlight(String company, int flightNumber) {
		return amf.cancelFlight(company, flightNumber);
	}

	public String boardFlight(String company, int flightNumber) {
		return amf.notifyBoarding(company, flightNumber);
	}

	public String changeFlightGate(String company, int flightNumber, String terminal, int gateNumber) {
		return amf.changeGate(company, flightNumber, terminal, gateNumber);
	}

}
