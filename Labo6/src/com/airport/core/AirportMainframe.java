package com.airport.core;

import com.airport.subject.Airport;
import com.airport.subject.Gate;
import com.airport.subject.Terminal;
import com.airport.util.Constant;

public class AirportMainframe {

	private Airport  airport = new Airport();
	private Terminal termA   = new Terminal(Constant.TERMINAL_A);
	private Terminal termB   = new Terminal(Constant.TERMINAL_B);
	private Terminal termC   = new Terminal(Constant.TERMINAL_C);
	private Gate[]   gatesA  = new Gate[Constant.SIZE_TERMINAL_A];
	private Gate[]   gatesB  = new Gate[Constant.SIZE_TERMINAL_B];
	private Gate[]   gatesC  = new Gate[Constant.SIZE_TERMINAL_C];

	public Airport  getAirport() { return airport; }
	public Terminal getTermA()   { return termA;   }
	public Terminal getTermB()   { return termB;   }
	public Terminal getTermC()   { return termC;   }
	public Gate[]   getGatesA()  { return gatesA;  }
	public Gate[]   getGatesB()  { return gatesB;  }
	public Gate[]   getGatesC()  { return gatesC;  }
	
	
	public String addFlight(Flight flight, String terminal, int gateNumber) {
		if(flightAlreadyExistInAirport(flight.getCompany(), flight.getFlightNumber())) {
			return Constant.ERROR_ADDFLIGHT_ALREADYEXIST;
		}
			
		if(isDoorAvailable(getRightGate(flight))) {
			airport.addFlight(flight);
			return addFlightToGate(flight, terminal, gateNumber);
		}
		
		return "The gate "+ flight.getGate() + Constant.ERROR_ADDFLIGHT_WRONGGATE;
	}
	
	private boolean flightAlreadyExistInAirport(String company, int flightNumber) {
		return (airport.getFlightById(company, flightNumber) != null);
	}
	
	public String addFlightToGate(Flight flight, String terminal, int gateNumber) {
		return addFlightToGate(flight, terminal, gateNumber, true);
	}
	
	public String addFlightToGate(Flight flight, String terminal, int gateNumber, boolean isDifferentTerminalDoor) {
		Terminal term = getRightTerminal(flight);
		if(term == null) { return Constant.ERROR; }
		
		if(isDifferentTerminalDoor) {
			term.addFlight(flight);
		} else {
			term.notifyObservers();
		}
		
		Gate gate = getRightGate(flight);
		if(gate == null) { return Constant.ERROR; }
		gate.addFlight(flight);
		
		return Constant.NO_ERROR;
	}
	
	/**
	 * Verify that no planes are boarding on the selected gate
	 * @param currentGate The gate to verify
	 * @return True if empty, false if there is already a plane boarding
	 */
	private boolean isDoorAvailable(Gate currentGate) {
		/* Pourquoi une liste s'il y a un maximum d'un avion? Je pr�f�re v�rifier si l'avion est en mode boarding */
		//return currentGate.getFlights().isEmpty();
		if(currentGate == null) {
			return false;
		}
		for(Flight temp : currentGate.getFlights()) {
			if(temp.getStatus().equals(Flight.BOARDING)) {
				return false;
			}
		}
		return true;
	}
	
	public String removeFlight(String flightCompany, int flightNumber) {
		Flight flightSearched = airport.getFlightById(flightCompany, flightNumber);
		
		if (flightSearched != null && !flightSearched.getTerminalFromGate().isEmpty()) {
			airport.removeFlight(flightSearched);
			removeFlightFromGate(flightSearched);
			return Constant.NO_ERROR;
		} else {
			return Constant.ERROR_FLIGHTNOTFOUND;
		}
	}
	
	private void removeFlightFromGate(Flight flightSearched) {
		removeFlightFromGate(flightSearched, true);
	}
	
	private void removeFlightFromGate(Flight flightSearched, boolean isDifferentTerminalDoor) {
		if(getRightTerminal(flightSearched) != null && getRightGate(flightSearched) != null) {
			if(isDifferentTerminalDoor) {
				getRightTerminal(flightSearched).removeFlight(flightSearched);				
			}
			getRightGate(flightSearched).removeFlight(flightSearched);			
		}
	}
	
	private Terminal getRightTerminal(Flight flight) {
		switch (flight.getTerminalFromGate()) {
		case Constant.TERM_A:
			return termA;
		case Constant.TERM_B:
			return termB;
		case Constant.TERM_C:
			return termC;
		}
		return null;
	}
	
	private Gate getRightGate(Flight flight) {
		return getRightGate(flight.getTerminalFromGate(), flight.getGateNumberFromGate());
	}
	
	public String delayFlight(String company, int flightNumber) {
		return changeFlightStatus(Flight.DELAYED, company, flightNumber);
	}

	public String cancelFlight(String company, int flightNumber) {
		return changeFlightStatus(Flight.CANCELLED, company, flightNumber);
	}
	
	public String notifyBoarding(String company, int flightNumber) {
		return changeFlightStatus(Flight.BOARDING, company, flightNumber);
	}
	
	private Gate getRightGate(String terminal, int gateNumber) {
		switch (terminal) {
		case Constant.TERM_A:
			if(gateNumber > Constant.SIZE_TERMINAL_A+1) {
				return null;
			}
			return gatesA[gateNumber - 1];
		case Constant.TERM_B:
			if(gateNumber > Constant.SIZE_TERMINAL_B+1) {
				return null;
			}
			return gatesB[gateNumber - 1];
		case Constant.TERM_C:
			if(gateNumber > Constant.SIZE_TERMINAL_C+1) {
				return null;
			}
			return gatesC[gateNumber - 1];
		}
		return null;
	}
	
	
	private String changeFlightStatus(String statusStr, String company, int flightNumber) {
		Flight flightSearched = airport.getFlightById(company, flightNumber);
		
		if (flightSearched == null) { return Constant.ERROR_FLIGHTNOTFOUND;	}		
		if (statusStr.isEmpty()) { return Constant.ERROR_CHANGESTATUS_NOTFOUND; }
		
		flightSearched.setStatus(statusStr);
		notifyMeMaster(flightSearched);
		return Constant.NO_ERROR;
	}
	
	/**
	 * Notify the airport, terminal and gate of a plane
	 * @param flight the flight
	 */
	private void notifyMeMaster(Flight flight) {
		airport.notifyObservers();
		getRightTerminal(flight).notifyObservers();
		getRightGate(flight).notifyObservers();
	}
	
	public String changeGate(String company, int flightNumber, String terminal, int gateNumber) {
		Flight flightSearched = airport.getFlightById(company, flightNumber);
		if (flightSearched == null) { return Constant.ERROR_FLIGHTNOTFOUND; }
		
		Gate currentGate = getRightGate(terminal, gateNumber);
		if(currentGate == null) { return "The gate "+ terminal + Constant.TERM_GATE_SEPERATOR + gateNumber + Constant.ERROR_GATEUSED; }
		if(sameGateBeforeAfter(terminal, gateNumber, flightSearched)) {	return Constant.ERROR_SAMEGATE;	}
		
		if(isDoorAvailable(currentGate)) {
			boolean isDifferentTerminal = true;
			if(flightSearched.getTerminalFromGate().equalsIgnoreCase(terminal)) {
				isDifferentTerminal = false;
			}
			
			removeFlightFromGate(flightSearched, isDifferentTerminal);
			flightSearched.setGate(terminal + Constant.TERM_GATE_SEPERATOR + gateNumber);
			airport.notifyObservers();
			return addFlightToGate(flightSearched, terminal, gateNumber, isDifferentTerminal);
			
		} else {
			return Constant.ERROR_GATENOTAVAILABLE;
		}
		
	}

	private boolean sameGateBeforeAfter(String terminal, int gateNumber, Flight flightSearched) {
		return (terminal + Constant.TERM_GATE_SEPERATOR + gateNumber).equalsIgnoreCase(flightSearched.getGate());

	}
}
