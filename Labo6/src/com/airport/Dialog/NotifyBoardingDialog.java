package com.airport.Dialog;

import java.awt.Container;
import java.awt.Dimension;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;

import javax.swing.BoxLayout;
import javax.swing.JButton;
import javax.swing.JLabel;
import javax.swing.JOptionPane;
import javax.swing.JPanel;
import javax.swing.JTextField;

import com.airport.core.Simulation;
import com.airport.util.Constant;

public class NotifyBoardingDialog extends AbstractAirportDialog{

	public static final int WIDTH = 300;
	public static final int HEIGHT = 350;
	
	public NotifyBoardingDialog(Simulation simulation) {
		super(simulation, Constant.SCREEN_NOTIFYBOARDING);
		
		okButton = new JButton(Constant.BUTTON_OKTEXT);
	    okButton.addActionListener(new ActionListener() {
	      public void actionPerformed(ActionEvent e) {
	        notifyBoarding();
	        setVisible(false);
	      }
	    });
	    
	    cancelButton = new JButton(Constant.BUTTON_CANCELTEXT);
	    cancelButton.addActionListener(new ActionListener() {
	      public void actionPerformed(ActionEvent e) {
	        int choice = JOptionPane.showConfirmDialog(
	          NotifyBoardingDialog.this,
	          Constant.CONFIRM_TEXT,
	          Constant.CANCEL_TEXT,
	          JOptionPane.YES_NO_OPTION);
	        
	        if (choice == JOptionPane.YES_OPTION) {
	          setVisible(false);
	        }
	      }
	    });
	    
	    JPanel companyPanel = new JPanel();
	    companyField = new JTextField(10);
	    companyPanel.add(new JLabel(Constant.DISPLAY_COMPANY));
	    companyPanel.add(companyField);
	    
	    JPanel flightNumberPanel = new JPanel();
	    flightNumberField = new JTextField(10);
	    flightNumberPanel.add(new JLabel(Constant.DISPLAY_FLIGHTNUMBER));
	    flightNumberPanel.add(flightNumberField);
	    
	    controlPanel = new JPanel();
	    controlPanel.setLayout(new BoxLayout(controlPanel, BoxLayout.X_AXIS));
	    controlPanel.add(okButton);
	    controlPanel.add(cancelButton);
	    
	    Container c = getContentPane();
	    c.setLayout(new BoxLayout(c,BoxLayout.Y_AXIS));
	    c.add(companyPanel);
	    c.add(flightNumberPanel);
	    c.add(controlPanel);
	    
	    setSize(new Dimension(WIDTH, HEIGHT));
	    setLocationRelativeTo(simulation);
	    
	}

	public void notifyBoarding() {
		String company = companyField.getText();
	    int flightNumber = Integer.parseInt(flightNumberField.getText());
	    
	    String errorMessage = simulation.boardFlight(company, flightNumber);
	    simulation.appendToDisplayTheResult(errorMessage, Constant.FLIGHT_STATUS_CHANGED);
	}
}
