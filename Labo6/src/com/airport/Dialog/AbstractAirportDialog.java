package com.airport.Dialog;

import javax.swing.JButton;
import javax.swing.JDialog;
import javax.swing.JPanel;
import javax.swing.JTextField;

import com.airport.core.Simulation;

public abstract class AbstractAirportDialog extends JDialog{

	protected Simulation simulation;
	protected JButton okButton;
	protected JButton cancelButton;
	protected JPanel controlPanel;
	protected JTextField companyField;
	protected JTextField flightNumberField;
	
	public AbstractAirportDialog(Simulation simulation, String text) {
		super(simulation, text);
		this.simulation = simulation;
	}
}
