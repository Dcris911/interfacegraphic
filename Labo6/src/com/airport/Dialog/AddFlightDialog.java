package com.airport.Dialog;
import java.awt.*;
import java.awt.event.*;
import javax.swing.*;
import javax.swing.event.*;

import com.airport.core.Flight;
import com.airport.core.Simulation;
import com.airport.util.Constant;

import javax.swing.border.*;
import java.util.*;
import java.io.*;

public class AddFlightDialog extends AbstractAirportDialog {
  
  public static final int WIDTH = 300;
  public static final int HEIGHT = 350;
  
  private String currentState = null;
  private JTextField destinationField, departureTimeField, terminalField, gateNumberField;
  private JComboBox statusBox;
  
  public AddFlightDialog(Simulation simulation) {
    
    super(simulation, Constant.SCREEN_ADDFLIGHT);
    this.simulation = simulation;
    
    okButton = new JButton(Constant.BUTTON_OKTEXT);
    okButton.addActionListener(new ActionListener() {
      public void actionPerformed(ActionEvent e) {
        addFlight();
        setVisible(false);
      }
    });
    
    cancelButton = new JButton(Constant.BUTTON_CANCELTEXT);
    cancelButton.addActionListener(new ActionListener() {
      public void actionPerformed(ActionEvent e) {
        int choice = JOptionPane.showConfirmDialog(
          AddFlightDialog.this,
          Constant.CONFIRM_TEXT,
          Constant.CANCEL_TEXT,
          JOptionPane.YES_NO_OPTION);
        
        if (choice == JOptionPane.YES_OPTION) {
          setVisible(false);
        }
      }
    });
    
    String[] statuses = { Flight.ONTIME, Flight.CANCELLED, Flight.BOARDING, Flight.DELAYED };
    JComboBox statusBox = new JComboBox(statuses);
    statusBox.setSelectedItem(Flight.ONTIME);
    statusBox.setEditable(false);
    statusBox.addActionListener(new ActionListener() {
      public void actionPerformed(ActionEvent e) {
        System.out.println((String)statusBox.getSelectedItem());
        currentState = (String)statusBox.getSelectedItem();
      }
    });
    
    JPanel companyPanel = new JPanel();
    companyField = new JTextField(10);
    companyPanel.add(new JLabel(Constant.DISPLAY_COMPANY));
    companyPanel.add(companyField);
    
    JPanel flightNumberPanel = new JPanel();
    flightNumberField = new JTextField(10);
    flightNumberPanel.add(new JLabel(Constant.DISPLAY_FLIGHTNUMBER));
    flightNumberPanel.add(flightNumberField);
    
    JPanel destinationPanel = new JPanel();
    destinationField = new JTextField(10);
    destinationPanel.add(new JLabel(Constant.DISPLAY_DESTINATION));
    destinationPanel.add(destinationField);
    
    JPanel departureTimePanel = new JPanel();
    departureTimeField = new JTextField(10);
    departureTimePanel.add(new JLabel(Constant.DISPLAY_DEPARTURETIME));
    departureTimePanel.add(departureTimeField);
    
    JPanel terminalPanel = new JPanel();
    terminalField = new JTextField(10);
    terminalPanel.add(new JLabel(Constant.DISPLAY_TERMINAL));
    terminalPanel.add(terminalField);
    
    JPanel gateNumberPanel = new JPanel();
    gateNumberField = new JTextField(10);
    gateNumberPanel.add(new JLabel(Constant.DISPLAY_GATENUMBER));
    gateNumberPanel.add(gateNumberField);
    
    JPanel flightStatusPanel = new JPanel();
    flightStatusPanel.add(new JLabel(Constant.DISPLAY_FLIGHTSTATUS));
    flightStatusPanel.add(statusBox);
    
    controlPanel = new JPanel();
    controlPanel.setLayout(new BoxLayout(controlPanel, BoxLayout.X_AXIS));
    controlPanel.add(okButton);
    controlPanel.add(cancelButton);
    
    Container c = getContentPane();
    c.setLayout(new BoxLayout(c,BoxLayout.Y_AXIS));
    c.add(companyPanel);
    c.add(flightNumberPanel);
    c.add(destinationPanel);
    c.add(departureTimePanel);
    c.add(terminalPanel);
    c.add(gateNumberPanel);
    c.add(flightStatusPanel);
    c.add(controlPanel);
    
    setSize(new Dimension(WIDTH, HEIGHT));
    setLocationRelativeTo(simulation);
  
  }
  
  public void addFlight() {
    
    String company = companyField.getText();
    int flightNumber = Integer.parseInt(flightNumberField.getText());
    String destination = destinationField.getText();
    int departureTime = Integer.parseInt(departureTimeField.getText());
    String terminal = terminalField.getText();
    int gateNumber = Integer.parseInt(gateNumberField.getText());
    if (currentState == null) {
      currentState = Flight.ONTIME;
    }
    String statusStr = currentState;
    
    Flight flight = new Flight(company, flightNumber, destination, departureTime, terminal + Constant.TERM_GATE_SEPERATOR + gateNumber, statusStr);
    
    String errorMessage = simulation.addFlight(flight, terminal, gateNumber);
    simulation.appendToDisplayTheResult(errorMessage, Constant.FLIGHT_ADDED + flight);
  }

}
