package com.airport.Dialog;

import java.awt.Container;
import java.awt.Dimension;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;

import javax.swing.BoxLayout;
import javax.swing.JButton;
import javax.swing.JLabel;
import javax.swing.JOptionPane;
import javax.swing.JPanel;
import javax.swing.JTextField;

import com.airport.core.Simulation;
import com.airport.util.Constant;

public class ChangeGateDialog extends AbstractAirportDialog{

	private JTextField terminalField, gateNumberField;
	public static final int WIDTH = 300, HEIGHT = 350;
	
	
	public ChangeGateDialog(Simulation simulation) {
		super(simulation, Constant.SCREEN_CHANGEGATE);

	    okButton = new JButton(Constant.BUTTON_OKTEXT);
	    okButton.addActionListener(new ActionListener() {
	      public void actionPerformed(ActionEvent e) {
	        changeFlight();
	        setVisible(false);
	      }
	    });
	    
	    cancelButton = new JButton(Constant.BUTTON_CANCELTEXT);
	    cancelButton.addActionListener(new ActionListener() {
	      public void actionPerformed(ActionEvent e) {
	        int choice = JOptionPane.showConfirmDialog(
	          ChangeGateDialog.this,
	          Constant.CONFIRM_TEXT,
	          Constant.CANCEL_TEXT,
	          JOptionPane.YES_NO_OPTION);
	        
	        if (choice == JOptionPane.YES_OPTION) {
	          setVisible(false);
	        }
	      }
	    });
	    
	    JPanel companyPanel = new JPanel();
	    companyField = new JTextField(10);
	    companyPanel.add(new JLabel(Constant.DISPLAY_COMPANY));
	    companyPanel.add(companyField);
	    
	    JPanel flightNumberPanel = new JPanel();
	    flightNumberField = new JTextField(10);
	    flightNumberPanel.add(new JLabel(Constant.DISPLAY_FLIGHTNUMBER));
	    flightNumberPanel.add(flightNumberField);
	    
	    JPanel terminalPanel = new JPanel();
	    terminalField = new JTextField(10);
	    terminalPanel.add(new JLabel(Constant.DISPLAY_NEWTERMINAL));
	    terminalPanel.add(terminalField);
	    
	    JPanel gateNumberPanel = new JPanel();
	    gateNumberField = new JTextField(10);
	    gateNumberPanel.add(new JLabel(Constant.DISPLAY_NEWGATE));
	    gateNumberPanel.add(gateNumberField);
	    
	    controlPanel = new JPanel();
	    controlPanel.setLayout(new BoxLayout(controlPanel, BoxLayout.X_AXIS));
	    controlPanel.add(okButton);
	    controlPanel.add(cancelButton);
	    
	    Container c = getContentPane();
	    c.setLayout(new BoxLayout(c,BoxLayout.Y_AXIS));
	    c.add(companyPanel);
	    c.add(flightNumberPanel);
	    c.add(terminalPanel);
	    c.add(gateNumberPanel);
	    c.add(controlPanel);
	    
	    setSize(new Dimension(WIDTH, HEIGHT));
	    setLocationRelativeTo(simulation);
	}

	protected void changeFlight() {
		String company = companyField.getText();
	    int flightNumber = Integer.parseInt(flightNumberField.getText());
	    String terminal = terminalField.getText();
	    int gateNumber = Integer.parseInt(gateNumberField.getText());
	    
	    String errorMessage = simulation.changeFlightGate(company, flightNumber, terminal, gateNumber);
	    simulation.appendToDisplayTheResult(errorMessage, Constant.FLIGHT_GATE_CHANGED);		
	}

}
