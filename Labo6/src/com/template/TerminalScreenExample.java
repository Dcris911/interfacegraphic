package com.template;
import java.util.List;

import com.airport.Dialog.ScreenDialog;

public class TerminalScreenExample extends /*Observer*/ Object {
  
  private /*Terminal*/String terminal;
  private String name;
  private ScreenDialog screenDialog;
  
  public TerminalScreenExample(/*Terminal*/String terminal, String name, ScreenDialog screenDialog) {
    this.terminal = terminal;
    this.name = name;
    this.screenDialog = screenDialog;
  }
  
  //@Override
  public void update() {
    /*List<Flight> flights = terminal.getFlights();
    Observer.displayFlights(flights, screenDialog);*/
  }

}
